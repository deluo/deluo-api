<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'department', 'assignee', 'owner', 'remarks', 'start_date', 'end_date', 'created_at', 'updated_at',
    ];

    public function department()
    {
        return $this->belongsTo('App\Department');
    }
}
