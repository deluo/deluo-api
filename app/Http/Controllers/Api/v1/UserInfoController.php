<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\User;
use App\ProfilePicture;
use App\Participant;
use App\AgeGroup;
use App\Role;
use Carbon\Carbon;
use App\Helpers\TokenHelperImpl;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserInfoController extends Controller
{

    private $tokenHelper;
    private $expire_date;
    private $now;
    private $sendEmail;

    public function __construct()
    {
        $this->expire_date = Carbon::now()->addDays(30);
        $this->now = Carbon::now();
        $this->sendEmail = false;
        $this->tokenHelper = new TokenHelperImpl();

    }


    /**
    * @param Request $request with user "email" and "password"
    *
    * Return the requesting user info
    *
    * @return \Illuminate\Http\Response
    */
    public function userInfo(Request $request)
    {
        if ($request->user()) {

            // return the userinfo with roles and agegroups appended
            return $request->user()->load('roles', 'ageGroups');

        } else {
            return response('Unauthorized',401);
        }
    }

    /**
    * @param Request $request with user "id" and "password"
    *
    * Return the requesting user info
    *
    * @return \Illuminate\Http\Response
    */
    public function participants(Request $request)
    {
        if ($request->user()) {
            return Participant::all();
        } else {
            return response('Unauthorized',401);
        }
    }


    public function storeParticipant(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'nickname' => 'required',
            'description' => 'nullable',
            'age' => 'nullable',
            'parent_id' => 'required'
        ]);

        if ($request->user()) {
            $participant = new Participant();

            $participant->firstname = $request->input('firstname');
            $participant->lastname = $request->input('lastname');
            $participant->nickname = $request->input('nickname');
            // $participant->age = $request->input('age');
            $participant->parent_id = $request->input('parent_id');
            $participant->save();

            return response('Successfully stored the Participant', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    public function updateParticipant(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'nickname' => 'required',
            'description' => 'nullable',
            'age' => 'nullable',
            'parent_id' => 'required'
        ]);

        if ($request->user()) {
            $participant = Participant::where('id', $request->id)->first();

            $participant->firstname = $request->input('firstname');
            $participant->lastname = $request->input('lastname');
            $participant->nickname = $request->input('nickname');
            // $participant->age = $request->input('age');
            $participant->parent_id = $request->input('parent_id');
            $participant->save();

            return response('Successfully updated the Participant', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    public function removeParticipant(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        if ($request->user()) {
            $participant = Participant::where('id', $request->id)->first();
            $participant->delete();

            return response('Successfully deleted the Participant', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    public function removePicture(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
        ]);

        if ($request->user()) {

            $user = User::where('id', $request->user()->id)->first();

            Storage::disk('public_path')->delete($user->profile_picture_url);

            $user->profile_picture_url = '';
            $user->save();

            return response('Successfully deleted the profilePicture', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

     /**
      * storing event picture
     *
     */
    public function uploadPicture(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
            'user_id' => 'required',
        ]);

        $user = User::where('id', $request->user_id)->first();

        $file = $request->file;
        if ($file->getClientSize() > 5242880) {
            return response("max file size 5 MB !!!", 422);
        }

        // $originalFileName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $token = $this->tokenHelper->getToken(10);

        $file_name = Carbon::now()->format('Y-m-d') . '_' . $token . '.' .$extension;

        $path = '/uploads_v1/profilepictures/profilepicture_' .  $user->id . '/';

        // first we store the file in temp folder
        // note : storage:put generates a unique name
        $shortFilePath = Storage::disk('public_path')->putFileAs($path, $file, $file_name);

        $user->profile_picture_url = $shortFilePath;
        $user->save();

        return $shortFilePath;
    }
}
