<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\User;
use App\Activity;
use App\Role;
use App\Absence;
use App\AgeGroup;
use Carbon\Carbon;

use App\Helpers\TokenHelperImpl;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AgeGroupController extends Controller
{
    public function __construct()
    {
        $this->tokenHelper = new TokenHelperImpl;
    }

    /**
    * @param Request $request
    *
    * Return the requesting user info
    *
    * @return \Illuminate\Http\Response
    */



    public function getById(Request $request)
    {
        $ageGroup = AgeGroup::where('id', $request->id)->first();
        // get all of the activities with the corresponding agegroup id
        $ageGroup['activities'] =  Activity::where('end_date', '>=', Carbon::now('Europe/Zurich'))
                                                ->whereHas('ageGroups', function ($query) use($ageGroup) {
                                                    $query->where('agegroup_id', $ageGroup->id);
                                                })->orderBy('end_date', 'asc')->get();
        //  get all the users with the corresponding agegroup id
        $ageGroup['users'] =  User::whereHas('ageGroups', function ($query) use($ageGroup) {
                                                    $query->where('age_group_id', $ageGroup->id);
                                                })->with('roles')->get();
        return $ageGroup;
    }

    public function getByIdWithLeaders(Request $request)
    {
        $ageGroup = AgeGroup::where('id', $request->id)->first();
        $leaderRole = Role::where('id', 8)->first();
        // get all of the activities with the corresponding agegroup id
        $ageGroup['activities'] =  Activity::where('end_date', '>=', Carbon::now('Europe/Zurich'))
                                                ->whereHas('ageGroups', function ($query) use($ageGroup) {
                                                    $query->where('agegroup_id', $ageGroup->id);
                                                })->orderBy('end_date', 'asc')->get();
        //  get all the users with the corresponding agegroup id and the role 8, which is the role for leader
        $ageGroup['users'] =  User::whereHas('ageGroups', function ($query) use($ageGroup) {
                                                $query->where('age_group_id', $ageGroup->id);
                                            })->whereHas('roles', function ($query) use($leaderRole) {
                                                $query->where('role_id', 8);
                                            })->with('roles')->get();
        return $ageGroup;
    }

    public function getAllAgeGroups(Request $request)
    {
        return AgeGroup::with('activities')->with('users')->with('users.roles')->with('users.ageGroups')->get();
        // return AgeGroup::load('activities', 'users', 'users.roles');
    }

    public function getActiveAgeGroups(Request $request)
    {
        return AgeGroup::with('activities')->with('users')->with('users.roles')->with('users.ageGroups')->where('active', 1)->get();
    }

    public function getNextGroupedByAgeGroup(Request $request)
    {
        $ageGroups = $this->ageGroups($request);

        foreach ($ageGroups as $ageGroup) {
            $ageGroup['activities'] =  Activity::where('end_date', '>=', Carbon::now('Europe/Zurich'))
                                                ->whereHas('ageGroups', function ($query) use($ageGroup) {
                                                    $query->where('agegroup_id', $ageGroup->id);
                                                })->get();
        }
        return $ageGroups;
    }

    public function getNextFiveGroupedByAgeGroup(Request $request)
    {
        $ageGroups = $this->ageGroups($request);

        foreach ($ageGroups as $ageGroup) {
            $ageGroup['activities'] =  Activity::where('end_date', '>=', Carbon::now('Europe/Zurich'))
                                                ->whereHas('ageGroups', function ($query) use($ageGroup) {
                                                    $query->where('agegroup_id', $ageGroup->id);
                                                })->take(5)->get();
        }
        return $ageGroups;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'nullable',
            'title' => 'required|string',
            'description' => 'required|string',
            'thumb_url' => 'required',
            'quarterly_plans' => 'nullable',
        ]);

        if ($request->user()) {
            $ageGroup = new AgeGroup();

            $ageGroup->name = $request->input('name');
            $ageGroup->title = $request->input('title');
            $ageGroup->description = $request->input('description');
            $ageGroup->thumb_url = $request->input('thumb_url');
            $ageGroup->quarterly_plans = $request->input('quarterly_plans');
            $ageGroup->style = 'background-color: #' . substr(md5(mt_rand()), 0, 6);
            $ageGroup->save();

            return response('Successfully stored the ageGroup', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }


     /**
      * storing agegroup Thumbnail
     *
     */
    public function uploadThumbnail(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);

        $file = $request->file;
        if ($file->getClientSize() > 10485760) {
            return response("Max. File Size 10 MB", 422);
        }

        // $originalFileName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $token = $this->tokenHelper->getToken(10);

        $file_name = Carbon::now()->format('Y-m-d') . '_' . $token . '.' .$extension;

        $path = '/uploads_v1/agegroups/thumbnails';

        // first we store the file in temp folder
        // note : storage:put generates a unique name
        $shortFilePath = Storage::disk('public_path')->putFileAs($path, $file, $file_name);

        return $shortFilePath;
    }

    /**
      * storing agegroup quarterly plans
     *
     */
    public function updateQuarterlyPlans(Request $request)
    {
        $this->validate($request, [
            'quarterlyPlansFile' => 'required',
        ]);

        $quarterlyPlansFile = $request->quarterlyPlansFile;
        if ($quarterlyPlansFile->getClientSize() > 10485760) {
            return response("Max. File Size 10 MB", 422);
        }

        // $originalFileName = $file->getClientOriginalName();
        $extension = $quarterlyPlansFile->getClientOriginalExtension();

        $token = $this->tokenHelper->getToken(10);

        $file_name = Carbon::now()->format('Y-m-d') . '_' . $token . '.' .$extension;

        $path = '/uploads_v1/agegroups/quarterly_plans';

        // first we store the file in temp folder
        // note : storage:put generates a unique name
        $shortFilePath = Storage::disk('public_path')->putFileAs($path, $quarterlyPlansFile, $file_name);

        return $shortFilePath;
    }

    /**
      * storing agegroup quarterly plans
     *
     */
    public function updateApplicationForm(Request $request)
    {
        $this->validate($request, [
            'applicationFormFile' => 'required',
        ]);

        $applicationFormFile = $request->applicationFormFile;
        if ($applicationFormFile->getClientSize() > 10485760) {
            return response("Max. File Size 10 MB", 422);
        }

        // $originalFileName = $file->getClientOriginalName();
        $extension = $applicationFormFile->getClientOriginalExtension();

        $token = $this->tokenHelper->getToken(10);

        $file_name = Carbon::now()->format('Y-m-d') . '_' . $token . '.' .$extension;

        $path = '/uploads_v1/agegroups/application_forms';

        // first we store the file in temp folder
        // note : storage:put generates a unique name
        $shortFilePath = Storage::disk('public_path')->putFileAs($path, $applicationFormFile, $file_name);

        return $shortFilePath;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'title' => 'required|string',
            'description' => 'required|string',
            'thumb_url' => 'required',
            'quarterly_plans' => 'nullable',
            'application_form' => 'nullable',
        ]);

        if ($request->user()) {
            $ageGroup = AgeGroup::where('id', $request->id)->first();

            $ageGroup->name = $request->input('name');
            $ageGroup->title = $request->input('title');
            $ageGroup->description = $request->input('description');
            $ageGroup->thumb_url = $request->input('thumb_url');
            $ageGroup->quarterly_plans = $request->input('quarterly_plans');
            $ageGroup->application_form = $request->input('application_form');
            $ageGroup->save();

            return response('Successfully updated the ageGroup', 200);
        } else {
            return response('Unauthorized', 401);
        }

    }

    public function toggleActive(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'active' => 'required',
        ]);

        if ($request->user()) {
            $ageGroup = AgeGroup::where('id', $request->id)->first();

            $ageGroup->active = $request->input('active');
            $ageGroup->save();

            return response('Successfully updated the ageGroup active', 200);
        } else {
            return response('Unauthorized', 401);
        }

    }

    public function remove(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        if ($request->user()) {
            $ageGroup = AgeGroup::where('id', $request->id)->with('activities')->first();

            foreach ($ageGroup->activities as $activity) {
                foreach ($activity->galleries as $gallery) {
                    foreach ($gallery->pictures as $picture) {
                        $picture->delete();
                    }
                    $gallery->delete();
                }
                foreach ($activity->absences as $absence) {
                    $absence->delete();
                }

                $activity->ageGroups()->detach();
            }

            foreach ($ageGroup->users as $user) {
                foreach ($user->roles as $role) {
                    $role->users()->detach();
                }
                $user->ageGroups()->detach();
            }

            $ageGroup->delete();

            return response('Successfully deleted the agegroup', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }
}