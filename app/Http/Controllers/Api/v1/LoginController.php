<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use Log;

class LoginController extends Controller
{
    private $expire_date;
    private $now;
    private $sendEmail;

    public function __construct()
    {
        $this->expire_date = Carbon::now()->addDays(30);
        $this->now = Carbon::now();
        $this->sendEmail = false;
    }

/**
 * login function
 *
 * @param Request $request with user "email" and "password"
 *
 * @return api_token on successful login
 */

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', $request->input('email'))->first();
        // $user = User::where('id', 1)->get();
        // return $user;

        if (!$user) {
            $res['success'] = false;
            $res['message'] = 'Your email or password is wrong';
            return response($res, 401);
        } else {
            if ($user->active == false){
                $res['success'] = false;
                $res['message'] = 'Your account has been deactivated';
                return response($res, 401);

            } else if (Hash::check($password, $user->password)) {
                $api_token = $this->generateAccessTokenAndStoreIt($user->id);
                $res['success'] = true;
                $res['api_token'] = $api_token;
                return response($res);

            } else {
                $res['success'] = false;
                $res['message'] = 'Your email or password is wrong';
                return response($res, 401);
            }
        }
    }

    /**
    * Return the requesting user info
    *
    * @return \Illuminate\Http\Response
    */
    public function userInfo(Request $request)
    {
        if ($request->user()) {
            return $request->user();
        } else {
            return response('Unauthorized',401);
        }
    }

    private function generateAccessTokenAndStoreIt($user_id)
    {
        $api_token = bin2hex(openssl_random_pseudo_bytes(64));

        DB::table('api_access_tokens')->insert(
        [
        'token' => $api_token,
        'user_id' =>  $user_id,
        'created_at' => $this->now,
        'expires_at' => $this->expire_date,
        ]
        );
        return $api_token;
    }

}
