<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\User;
use App\Activity;
use App\Role;
use App\Absence;
use App\AgeGroup;
use Carbon\Carbon;

use App\Helpers\TokenHelperImpl;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->tokenHelper = new TokenHelperImpl;
    }

    /**
    * @param Request $request
    *
    * Return the requesting user info
    *
    * @return \Illuminate\Http\Response
    */



    public function getById(Request $request)
    {
        $role = Role::where('id', $request->id)->first();

        //  get all the users with the corresponding agegroup id
        $role['users'] =  User::whereHas('roles', function ($query) use($role) {
                                                    $query->where('role_id', $role->id);
                                                })->get();
        return $role;
    }

    public function getAllRoles(Request $request)
    {
        return Role::with('users')->get();
    }

    public function getActiveRoles(Request $request)
    {
        return Role::with('users')->where('active', 1)->get();
    }

    public function getActiveDisplayedRoles(Request $request)
    {
        return Role::with('users')->where('active', 1)->where('display', 1)->get();
    }

    public function getLeadersByActiveRole(Request $request)
    {
        $roles = Role::where('active', 1)->get();
        foreach ($roles as $role) {
            $role['users'] =  User::whereHas('roles', function ($query) use($role) {
                $query->where('role_id', 8);
            })->get();
        }
        return $roles;
    }

    public function getLeadersByActiveDisplayRole(Request $request)
    {
        $roles = Role::where('active', 1)->where('display', 1)->get();
        foreach ($roles as $role) {
            $role['users'] =  User::whereHas('roles', function ($query) use($role) {
                $query->where('role_id', 8);
            })->get();
        }
        return $roles;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        if ($request->user()) {
            $role = new Role();

            $role->name = $request->input('name');
            $role->description = $request->input('description');

            $role->save();
            return response('Successfully stored the role', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        if ($request->user()) {
            $role = Role::where('id', $request->id)->first();

            $role->name = $request->input('name');
            $role->description = $request->input('description');
            $role->save();

            return response('Successfully updated the role', 200);
        } else {
            return response('Unauthorized', 401);
        }

    }

    public function toggleActive(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'active' => 'required',
        ]);

        if ($request->user()) {
            $role = Role::where('id', $request->id)->first();

            $role->active = $request->input('active');
            $role->save();

            return response('Successfully updated the role active', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    public function toggleDisplay(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'display' => 'required',
        ]);

        if ($request->user()) {
            $role = Role::where('id', $request->id)->first();

            $role->display = $request->input('display');
            $role->save();

            return response('Successfully updated the role display', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    public function remove(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        if ($request->user()) {
            $role = Role::where('id', $request->id)->first();
            $role->users()->detach();
            $role->delete();

            return response('Successfully deleted the role', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }
}