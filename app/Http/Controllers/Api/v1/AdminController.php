<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\User;
use App\Content;
use App\AgeGroup;
use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function getAllUsersGroupedByAgeGroup(Request $request)
    {
        $ageGroups = $this->ageGroups($request);

        foreach ($ageGroups as $ageGroup) {
            $ageGroup['users'] =  User::where('age_group_id', $ageGroup->id)
                                        ->get();
        }
        return $ageGroups;
    }

    public function ageGroups(Request $request)
    {
        return AgeGroup::all();
    }

    public function getAllLeaders(Request $request)
    {
        $ageGroups = $this->activeAgeGroups($request);

        foreach ($ageGroups as $ageGroup) {
            $ageGroup['users'] =  User::whereHas('roles', function ($query) {
                                                    $query->where('role_id','=', 5)
                                                    ->with('roles');
                                                })
                                                ->where('age_group_id', $ageGroup->id)
                                                ->get();
        }
        return $ageGroups;
    }

    public function activeAgeGroups(Request $request)
    {
        return AgeGroup::where('active', true)->where('name', '!=', 'leiter')->get();
    }

    public function toggleActive(Request $request)
    {
        if ($request->user()) {
            $user = User::where('id', $request->id)->first();
            $user->active = !$user->active;
            $user->save();
            return response('Successfully toggled the User', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }

    public function switchRole(Request $request)
    {
        if ($request->user()) {
            $user = User::where('id', $request->id)->first();
            $user->role_id = $request->role_id;
            $user->save();
            return response('Successfully updated the Role', 200);
        } else {
            return response('Unauthorized', 401);
        }
    }
}