<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'title', 'description', 'entrance_year', 'token', 'created_at', 'updated_at',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'role_user','role_id', 'user_id');
    }

    public function assignments() {
        return $this->hasMany('App\User', 'role_user','role_id', 'user_id');
    }
}
