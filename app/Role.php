<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'description', 'active', 'created_at', 'updated_at',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'role_user','role_id', 'user_id');
    }
}
