<?php

namespace App\Helpers;

class TokenHelperImpl implements TokenHelper
{

    /**
     * Generates a crypto random number between a min and a max value
     *
     * @param   int $min Minimum value
     * @param   int $max Maximum value
     *
     * @return  A truly random number
     */
    function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    /**
     * Generates the token by a given length
     *
     * @param   int $length Length of the token
     *
     * @return  string Returns the generated token
     */
    function getToken($length)
    {
        $token = "";
        $codeAlphabet = "abcdefghijkmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }
}

?>
