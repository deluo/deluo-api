<?php

namespace App\Helpers;

interface TokenHelper
{
    /**
     * Generates a crypto random number between a min and a max value
     *
     * @param   int $min Minimum value
     * @param   int $max Maximum value
     *
     * @return  A truly random number
     */
    function crypto_rand_secure($min, $max);

    /**
     * Generates the token by a given length
     *
     * @param   int $length Length of the token
     *
     * @return  string Returns the generated token
     */
    function getToken($length);
}

?>
