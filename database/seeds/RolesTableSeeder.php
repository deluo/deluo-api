<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = New Role;
        $role->name = 'Admin';
        $role->description = 'Admin user';
        $role->save();

        $role = New Role;
        $role->name = 'Student';
        $role->description = 'Student user';
        $role->save();

        $role = New Role;
        $role->name = 'Teacher';
        $role->description = 'Teacher user';
        $role->save();
    }
}
