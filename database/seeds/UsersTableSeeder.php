<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = App\Role::all();

        $user = New User;
        $user->firstname = 'Mark';
        $user->lastname = 'Marolf';
        $user->email = 'dc3dude@gmail.com';
        $user->password = app('hash')->make('mynameisjeff');
        $user->save();

        // $user = User::where('id', 1);

        App\User::all()->each(function ($user) use ($roles) {
            $user->roles()->attach(
                $roles->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}