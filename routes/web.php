<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function ($app) use($router) {
    $router->group(['namespace' => 'Api'], function() use ($router)
    {
        // Api v1
        $router->group(['prefix' => 'v1'], function ($app) use($router) {
            $router->group(['namespace' => 'v1'], function() use ($router)
            {
                $router->get('/', function () use ($router) {
                    return "api/v1";
                });

                $router->group(['prefix' => 'absence'], function () use($router) {
                    $router->post('store','AbsenceController@store');
                    $router->post('update','AbsenceController@update');
                    $router->post('remove','AbsenceController@remove');
                    $router->get('getAll','AbsenceController@getAll');
                    $router->get('getByActivityId','AbsenceController@getByActivityId');
                });

                $router->group(['prefix' => 'account'], function () use($router) {
                    $router->post('updatePassword','ProfileController@updatePassword');
                    $router->post('updateEmail','ProfileController@updateEmail');
                    $router->post('updateName','ProfileController@updateName');
                });

                $router->group(['prefix' => 'activities'], function () use($router) {
                    $router->get('/','ActivityController@getAllActivities');
                    $router->get('/next','ActivityController@getNextActivities');
                    $router->get('/getNextGroupedByAgeGroup','ActivityController@getNextGroupedByAgeGroup');
                    $router->get('/getNextFiveGroupedByAgeGroup','ActivityController@getNextFiveGroupedByAgeGroup');

                    $router->get('/getById','ActivityController@getById');
                    $router->get('/ageGroups','ActivityController@ageGroups');
                    $router->post('store','ActivityController@store');
                    $router->post('update','ActivityController@update');
                    $router->post('/remove','ActivityController@remove');
                });

                $router->group(['prefix' => 'admin'], function () use($router) {
                    $router->get('/getAll','AdminController@getAll');
                    $router->get('/getAllUsersGroupedByAgeGroup','AdminController@getAllUsersGroupedByAgeGroup');
                    $router->get('/getAllLeaders','AdminController@getAllLeaders');
                    $router->get('/ageGroups','AdminController@activeAgeGroups');
                    $router->post('/active','AdminController@toggleActive');
                    $router->post('/role','AdminController@switchRole');
                });

                $router->group(['prefix' => 'agegroups'], function () use($router) {
                    $router->get('getById','AgeGroupController@getById');
                    $router->get('getByIdWithLeaders','AgeGroupController@getByIdWithLeaders');
                    $router->get('getAllAgeGroups','AgeGroupController@getAllAgeGroups');
                    $router->get('getActiveAgeGroups','AgeGroupController@getActiveAgeGroups');
                    $router->get('getNextGroupedByAgeGroup','AgeGroupController@getNextGroupedByAgeGroup');
                    $router->get('getNextFiveGroupedByAgeGroup','AgeGroupController@getNextFiveGroupedByAgeGroup');
                    $router->post('store','AgeGroupController@store');
                    $router->post('uploadThumbnail','AgeGroupController@uploadThumbnail');
                    $router->post('updateQuarterlyPlans','AgeGroupController@updateQuarterlyPlans');
                    $router->post('updateApplicationForm','AgeGroupController@updateApplicationForm');
                    $router->post('update','AgeGroupController@update');
                    $router->post('toggleActive','AgeGroupController@toggleActive');
                    $router->post('remove','AgeGroupController@remove');
                });

                $router->group(['prefix' => 'auth'], function () use($router){
                    $router->post('login','LoginController@login');
                    $router->post('register','AuthController@register');
                    $router->post('logout','AuthController@logout');
                    $router->post('forgetPassword','AuthController@forgetPassword');
                    $router->post('resetPassword','AuthController@resetPassword');
                    $router->post('emailConfirmation','AuthController@emailConfirmation');
                    $router->post('changeAccountInfo','AuthController@changeAccountInfo');
                    $router->post('toggleActive','AuthController@toggleActive');
                    $router->post('toggleRole','AuthController@toggleRole');
                    $router->post('toggleAgeGroup','AuthController@toggleAgeGroup');

                    $router->get('userinfo','UserInfoController@userinfo');
                    $router->post('uploadPicture','UserInfoController@uploadPicture');
                    $router->post('removePicture','UserInfoController@removePicture');
                    $router->post('storeParticipant','UserInfoController@storeParticipant');
                    $router->post('updateParticipant','UserInfoController@updateParticipant');
                    $router->post('removeParticipant','UserInfoController@removeParticipant');
                    $router->get('participants','UserInfoController@participants');
                });

                $router->group(['prefix' => 'contents'], function () use($router) {
                    $router->get('/','ContentController@getAll');
                    $router->get('getContentById','ContentController@getContentById');
                    $router->get('getNextContents','ContentController@getNextContents');
                    $router->get('getThumbnailById','ContentController@getThumbnailById');
                    $router->post('store','ContentController@store');
                    $router->post('update','ContentController@update');
                    $router->post('remove','ContentController@remove');
                    $router->post('uploadThumbnail','ContentController@uploadThumbnail');
                    $router->post('removeThumbnail','ContentController@removeThumbnail');
                });

                $router->group(['prefix' => 'documents'], function () use($router) {
                    $router->get('/getAll','DocumentController@getAll');
                    $router->get('/getAllActive','DocumentController@getAllActive');
                    $router->get('/getById','DocumentController@getById');
                    $router->post('/store','DocumentController@store');
                    $router->post('/storeFile','DocumentController@storeFile');
                    $router->post('/update','DocumentController@update');
                    $router->post('/toggleActive','DocumentController@toggleActive');
                    $router->post('/remove','DocumentController@remove');
                });

                // email
                 $router->group(['prefix' => 'email'], function () use($router) {
                    $router->post('send','EmailController@sendContactEmail');
                });

                // Galleries
                 $router->group(['prefix' => 'galleries'], function () use($router) {
                    $router->get('/','GalleryController@getAll');
                    $router->get('getById','GalleryController@getById');
                    $router->post('store','GalleryController@store');
                    $router->post('update','GalleryController@update');
                    $router->post('uploadPicture','GalleryController@uploadPicture');
                    $router->post('removeGallery','GalleryController@removeGallery');
                    $router->post('removePicture','GalleryController@removePicture');
                    $router->post('removeAllPictures','GalleryController@removeAllPictures');
                });
                
                // roles
                $router->group(['prefix' => 'roles'], function () use($router) {
                $router->get('getById','AgeGroupController@getById');
                $router->get('getAllRoles','RoleController@getAllRoles');
                $router->get('getActiveRoles','RoleController@getActiveRoles');
                $router->get('getActiveDisplayedRoles','RoleController@getActiveDisplayedRoles');
                $router->get('getLeadersByRole','RoleController@getLeadersByRole');
                $router->get('getLeadersByActiveRole','RoleController@getLeadersByActiveRole');
                $router->get('getLeadersByActiveDisplayRole','RoleController@getLeadersByActiveDisplayRole');
                $router->post('store','RoleController@store');
                $router->post('update','RoleController@update');
                $router->post('toggleActive','RoleController@toggleActive');
                $router->post('toggleDisplay','RoleController@toggleDisplay');
                $router->post('remove','RoleController@remove');
                });
            });
        });
    });
});